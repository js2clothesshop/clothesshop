var express = require('express');
var bodyParser = require('body-parser');
var PORT = process.env.PORT || 3000;
var app = express();
var path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();

});

var sizes = ['XS', 'S', 'M', 'L', 'XL'];
var colors = ['biały', 'czarny', 'czerwony', 'różowy', 'niebieski', 'zielony', 'żółty', 'inny'];

var outfits = [
    {id: 1, name: 'Koszula', size: sizes[0], color: colors[1], price: 45.99, category: [2, 6]},
    {id: 2, name: 'Spodnie', size: sizes[3], color: colors[4], price: 89.00, category: [1, 5]},
    {id: 3, name: 'Spódnica', size: sizes[1], color: colors[3], price: 89.00, category: [1, 6]}

];

var categories = [
    {id: 1, name: 'damskie'},
    {id: 2, name: 'męskie'},
    {id: 3, name: 'dziecięce'},
    {id: 4, name: 'zimowe'},
    {id: 5, name: 'letnie'},
    {id: 6, name: 'codzienne'},
    {id: 7, name: 'wieczorowe'}
];




var currentOutfitId = 3;
var currentCategoryId = 8;

//home
app.get('/', function (req, res) {
    res.sendFile('./public/index.html', {root: __dirname});

});

// --- outfits ---

//READ
app.get('/outfits', function (req, res) {
    res.json({outfits: outfits, categories: categories, sizes: sizes, colors: colors});

});

//CREATE
app.post('/outfits', function (req, res) {
    var outfit = req.body;
    outfit.id = currentOutfitId++;
    outfits.push(outfit);
    res.json({outfits: outfits});

});

//DELETE
app.delete('/outfits/:id', function (req, res) {
    console.log('Usuwanie ubrania ' + req.params.id);
    var outfit = findElement(parseInt(req.params.id), outfits);
    if (outfit === null) {
        res.send(404);
    }
    else {
        removeElement(parseInt(req.params.id, 10), outfits);
        res.json({outfits: outfits});

    }
});

//UPDATE
app.put('/outfits/:id', function (req, res) {
    var outfit = req.body;
    var currentOutfit = findElement(parseInt(req.params.id), outfits);
    if (currentOutfit === null) {
        res.send(404);
    }
    else {
        currentOutfit.name = outfit.name;
        currentOutfit.color = outfit.color;
        currentOutfit.size = outfit.size;
        currentOutfit.price = outfit.price;
        currentOutfit.category = outfit.category;
        res.send({outfits: outfits});
    }
});


// --- categories ---

//READ
app.get('/categories', function (req, res) {
    res.json({categories: categories});

});

//CREATE
app.post('/categories', function (req, res) {
    var category = req.body;
    category.id = currentCategoryId++;
    categories.push(category);
    res.json({categories: categories});

});

//DELETE
app.delete('/categories/:id', function (req, res) {
    console.log('Usuwanie kategorii ' + req.params.id);
    var category = findElement(parseInt(req.params.id), categories);
    if (category === null) {
        res.send(404);
    }
    else {
        removeElement(parseInt(req.params.id, 10), categories);
        res.json({categories: categories});

    }
});

//UPDATE
app.put('/categories/:id', function (req, res) {
    var outfit = req.body;
    console.log('Edycja ubrania: ' + req.params.id);
    var currentCategory = findElement(parseInt(req.params.id), categories);
    if (currentCategory === null) {
        res.send(404);
    }
    else {
        currentCategory.name = outfit.name;
        res.send({categories: categories});
    }
});


app.listen(PORT, function () {
    console.log('Listening on port ' + PORT);
});


function findElement(id, elementsArray) {

    for (var i = 0; i < elementsArray.length; i++) {
        if (elementsArray[i].id === id) {
            return elementsArray[i];
        }
    }
    return null;
}

function removeElement(id, elementsArray) {
    var elementIndex = 0;
    for (var i = 0; i < elementsArray.length; i++) {
        if (elementsArray[i].id === id) {
            elementIndex = i;
        }
    }
    elementsArray.splice(elementIndex, 1);
}
