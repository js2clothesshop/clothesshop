var main_background_color = '#F3EEDF';
var light_accent_color = '#D9B6CF';
var dark_accent_color = '#594154';
var medium_accent_color = '#BD6895';
var contrast_color = '#EBC4B6';


$(document).ready(function () {

    //dropdowns


    $('#multiselect-size').hover(function () {
        $("#size-options").slideDown('fast');

    }, function () {
        $("#size-options").slideUp('fast');


    });

    $('#multiselect-color').hover(function () {
        $('#color-options').slideDown('fast');

    }, function () {
        $('#color-options').slideUp('fast');

    });

    $('#multiselect-category').hover(function () {
        $('#category-options').slideDown('fast');

    }, function () {
        $('#category-options').slideUp('fast');
    });

    $('#multiselect-size-add').hover(function () {
        $('#size-options-add').slideDown('fast');

    }, function () {
        $('#size-options-add').slideUp('fast');

    });


    $('#multiselect-category-add').hover(function () {
        $("#category-options-add").slideDown('fast');

    }, function () {
        $("#category-options-add").slideUp('fast');


    });

    $('#filter-tab').click(function () {
        $('#filter').css('display', 'block');
        $('#add-product').css('display', 'none');
        $(this).addClass('active').css('background-color', medium_accent_color);
        $('#add-product-tab').removeClass('active').css('background-color', light_accent_color);

    });

    $('#add-product-tab').click(function () {

        $('#add-product').css('display', 'block');
        $('#filter').css('display', 'none');
        $(this).addClass('active').css('background-color', medium_accent_color);
        $('#filter-tab').removeClass('active').css('background-color', light_accent_color);


    });

    $('#name-input').keyup(function () {
       if ($(this).val()){
           $('#name-empty-span').css('display', 'none');
       }
    });

    $('#price-input').keyup(function () {
        if ($(this).val()){
            $('#price-empty-span').css('display', 'none');
        }
    });

    $('#size-input').click(function () {
        if ($(this).val()){
            $('#size-empty-span').css('display', 'none');
        }
    });

    $('#color-input').click(function () {
        if ($(this).val()){
            $('#color-empty-span').css('display', 'none');
        }
    });

    $('.category-label').change(function () {
       if (getCategories().length !== 0){
           $('#category-empty-span').css('display', 'none');

       }
    });




    $('#search').keyup(filter);

    $('#price-min').keyup(filter);
    $('#price-max').keyup(filter);



    $('input[type=checkbox]').click(function () {
        filter();
    });

    function getCategories() {
        var selected = [];
        $('#category-options-add').find('input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        return selected;
    }

    function filter() {
        var rows = $("#clothes-tbody").find("tr");
        rows.hide();

        rows.filter(function () {
            var result = true;

            //search name
            var rex = new RegExp($('#search').val(), 'i');
            var nameCell = $(this).find("td:eq(0)");
            result = rex.test(nameCell.text());

            //filter size
            var sizeCell = $(this).find("td:eq(1)");
            result = result && sizeFilter(sizeCell);

            //filter color
            var colorCell = $(this).find("td:eq(2)");
            result = result && colorFilter(colorCell);

            //filter category
            var categoryCell = $(this).find("td:eq(4)");
            result = result && categoryFilter(categoryCell);

            var priceCell = $(this).find("td:eq(3)");
            result = result && priceFilter(priceCell);

            return result;

        }).show();

    }

    function sizeFilter(cell) {
        if ($("#size-options :checkbox:checked").length == 0) {
            return true;
        }
        var success = false;
        $("#size-options :checkbox:checked").each(function () {
            if (cell.text() === $(this).val()) {
                success = true;
            }
        });
        return success;
    }

    function colorFilter(cell) {
        if ($("#color-options :checkbox:checked").length == 0) {
            return true;
        }
        var success = false;
        $("#color-options :checkbox:checked").each(function () {
            if (cell.text() === $(this).val()) {
                success = true;
            }
        });
        return success;
    }


    function categoryFilter(cell) {
        if ($("#category-options :checkbox:checked").length == 0) {
            return true;
        }

        var success = false;
        var cellCategories = [];
        var cellListLength = $(cell).find("li").length;

        //find all categories from cell
        for (var i = 0; i < cellListLength; i++) {
            var cat = $(cell).find("li").eq(i).text();
            cellCategories.push(cat);

        }
        $("#category-options :checkbox:checked").each(function () {

            for (var cat in cellCategories) {
                if (cellCategories[cat] === $(this).val()) {
                    success = true;
                }
            }
        });
        return success;
    }

    function priceFilter(cell) {
        var success = true;
        var priceMin = $('#price-min').val();
        var priceMax = $('#price-max').val();
        var cellPrice = parseFloat(cell.text());

        if (priceMin && cellPrice < parseFloat(priceMin)) {
            success = false;
        } else if (priceMax && cellPrice > parseFloat(priceMax)) {
            success = false;
        }
        return success;

    }
});


