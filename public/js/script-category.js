var main_background_color = '#F3EEDF';
var light_accent_color = '#D9B6CF';
var dark_accent_color = '#594154';
var medium_accent_color = '#BD6895';
var contrast_color = '#EBC4B6';

$(document).ready(function () {



    $('#add-category-tab').click(function () {

        $('#add-category').css('display', 'block');
        $(this).addClass('active').css('background-color', medium_accent_color);
    });

    $('#search').keyup(filter);

    $('#name-input').keyup(function () {
        if ($(this).val()){
            $('#name-empty-span').css('display', 'none');
        }
    });

    // $('#edit-category').blur(function () {
    //     if ($(this).val() == ''){
    //         $('#error-name-info').css('display', 'block');
    //     }

});


    function filter() {
        var rows = $("#categories-tbody").find("tr");
        rows.hide();


        rows.filter(function () {
            var result = true;

            //search name
            var rex = new RegExp($('#search').val(), 'i');
            var nameCell = $(this).find("td:eq(0)");
            result = rex.test(nameCell.text());

            return result;

        }).show();

    }


