var app = angular.module('clothes-shop', []);


app.controller('CategoryController', function ($scope, $http) {

    $scope.categories = {};

    $scope.category = {};
    $scope.editedCategory = {};


    $http.get("http://localhost:3000/categories")
        .then(function (response) {
            $scope.categories = response.data.categories;

            $scope.add = function (categories) {
                if ($scope.isAllDataValid()) {
                    $http.post("http://localhost:3000/categories", this.category)
                        .then(function (response) {
                                $scope.categories = response.data.categories;
                                console.log("Category addeds");
                            },
                            function (response) {
                                console.log("ERROR - category add")
                            });

                    this.category = {};
                    $scope.categoryForm.$setUntouched();
                }
            };

            $scope.delete = function (category) {
                var id = category.id;

                $http.delete("http://localhost:3000/categories/" + id, {params: {id: id}})
                    .then(function (response) {
                        console.log("Category deleted");
                        $scope.categories = response.data.categories;

                    }, function (response) {
                        console.log("ERROR - category delete");
                    })

            };

            $scope.saveEdited = function () {

                if (!$scope.isEmptyName()) {

                    var id = $scope.editedCategory.id;
                    $http.put("http://localhost:3000/categories/" + id, $scope.editedCategory, {params: {id: id}})
                        .then(function (response) {
                            console.log("Category edited");
                            $scope.categories = response.data.categories;
                        }, function (response) {
                            console.log("ERROR - category edit")
                        });

                    $scope.reset();

                }
            };


        });


    $scope.edit = function (category) {

        $scope.editedCategory = angular.copy(category);

    };

    $scope.reset = function () {
        $scope.editedCategory = {};
    };

    $scope.getTemplate = function (category) {
        if (category.id === $scope.editedCategory.id) return 'edit';
        else return 'display';
    };

    $scope.isEmptyName = function () {
        return ($('#edit-category').val() === '');
    };

    $scope.isAnotherNameError = function () {
        return $('#name-empty-span').css('display') === 'block';
    };

    $scope.isAllDataValid = function () {
        var result = true;

        // name validation
        if (!$('#name-input').val()) {
            if ($('#name-error-span').css('display') === 'none') {
                $('#name-empty-span').css('display', 'block');
            } else {
                $('#name-empty-span').css('display', 'none');

            }
            result = false;

        }
        else {
            $('#name-empty-span').css('display', 'none');

        }

        return result;
    };


    $scope.sortType = 'name';
    $scope.sortReverse = false;

});