var app = angular.module('clothes-shop', []);

app.controller('OutfitController', function ($scope, $http) {

    $scope.outfits = {};
    $scope.categories = {};
    $scope.sizes = {};
    $scope.colors = {};

    $scope.outfit = {};
    $scope.editedOutfit = {};
    $scope.editedColors = [];
    $scope.editedSizes = [];

    $http.get("http://localhost:3000/outfits")
        .then(function (response) {
            $scope.outfits = response.data.outfits;
            $scope.categories = response.data.categories;
            $scope.sizes = response.data.sizes;
            $scope.colors = response.data.colors;

            $scope.add = function (outfits) {
                if ($scope.isAllDataValid()) {

                    $scope.outfit.category = $scope.getCategories();
                    $http.post("http://localhost:3000/outfits", $scope.outfit)
                        .then(function (response) {
                                console.log("Outfit added");
                                $scope.outfits = response.data.outfits;
                            },
                            function (response) {
                                console.log("ERROR - outfit add")
                            });

                    this.outfit = {};
                    $scope.outfitForm.$setUntouched();

                }
            };
            $scope.delete = function (outfit) {
                var id = outfit.id;

                $http.delete("http://localhost:3000/outfits/" + id, {params: {id: id}})
                    .then(function (response) {
                        console.log("Outfit deleted");
                        $scope.outfits = response.data.outfits;

                    }, function (response) {
                        console.log("ERROR - outfit delete");
                    })
            };


            $scope.saveEdited = function (editedOutfit) {

                if ($scope.isValidData()) {
                    var id = editedOutfit.id;


                    $http.put("http://localhost:3000/outfits/" + id, $scope.editedOutfit, {params: {id: id}})
                        .then(function (response) {
                            console.log("Outfit edited");
                            $scope.outfits = response.data.outfits;
                        }, function (response) {
                            console.log("ERROR - outfit edit")
                        });

                    $scope.reset();

                }
            }

        });

    $scope.isAllDataValid = function () {
        var result = true;

        // name validation
        if (!$('#name-input').val()) {
            if ($('#name-error-span').css('display') === 'none') {
                $('#name-empty-span').css('display', 'block');
            } else {
                $('#name-empty-span').css('display', 'none');

            }
            result = false;

        }
        else {
            $('#name-empty-span').css('display', 'none');

        }

        // size validation
        if (!$('#size-input').val()) {
            if ($('#size-error-span').css('display') === 'none') {
                $('#size-empty-span').css('display', 'block');
            } else {
                $('#size-empty-span').css('display', 'none');

            }
            result = false;

        }
        else {
            $('#size-empty-span').css('display', 'none');

        }

        // category validation
        if ($scope.getCategories().length === 0) {
            $('#category-empty-span').css('display', 'block');
            result = false;

        } else {
            $('#category-empty-span').css('display', 'none');

        }

        // color validation
        if (!$('#color-input').val()) {
            if ($('#color-error-span').css('display') === 'none') {
                $('#color-empty-span').css('display', 'block');
            } else {
                $('#color-empty-span').css('display', 'none');

            }
            result = false;

        }
        else {
            $('#color-empty-span').css('display', 'none');

        }

        // price validation
        if (!$('#price-input').val()) {
            if ($('#price-error-span').css('display') === 'none') {
                $('#price-empty-span').css('display', 'block');
            } else {
                $('#price-empty-span').css('display', 'none');

            }
            result = false;

        }
        else {
            $('#price-empty-span').css('display', 'none');

        }

        return result;
    };


    $scope.edit = function (outfit) {

        $scope.editedOutfit = angular.copy(outfit);
        $scope.getEditedSizes();
        $scope.getEditedColors();

    };

    $scope.reset = function () {
        $scope.editedOutfit = {};
        $scope.editedSizes = [];
        $scope.editedColors = [];
    };

    $scope.getTemplate = function (outfit) {
        if (outfit.id === $scope.editedOutfit.id) return 'edit';
        else return 'display';
    };

    $scope.getCategories = function () {
        var selected = [];
        $('#category-options-add').find('input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
        return selected;
    };

    $scope.getEditedSizes = function () {

        $scope.editedSizes = $scope.sizes.filter(function (size) {
            return !(size === $scope.editedOutfit.size);
        });

    };
    $scope.getEditedColors = function () {

        $scope.editedColors = $scope.colors.filter(function (color) {
            return !(color === $scope.editedOutfit.color);
        });

    };

    $scope.getCategoryNameById = function (id) {
        for (var i = 0; i < $scope.categories.length; i++) {
            if ($scope.categories[i].id === id) {
                return $scope.categories[i].name;
            }
        }
        return null;
    };

    $scope.isValidData = function () {
        return !$scope.isEmptyName() && !$scope.isInvalidPrice();
    };

    $scope.isEmptyName = function () {
        return ($('#edit-name').val() === '');
    };

    $scope.isInvalidPrice = function () {
        return ($('#edit-price').val() === '' || $('#edit-price').val().substr(0, 1) === '-');
    };

    $scope.isEmptyCategorySelect = function () {
        var sizeCheckedLength = $('#category-options-add').find('input:checked').length;
        return $('#outfitCategory').$touched && sizeCheckedLength === 0;

    };

    $scope.isAnotherNameError = function () {
        return $('#name-empty-span').css('display') === 'block';
    };

    $scope.isAnotherPriceError = function () {
        return $('#price-empty-span').css('display') === 'block';
    };

    $scope.isAnotherColorError = function () {
        return $('#color-empty-span').css('display') === 'block';
    };

    $scope.isAnotherSizeError = function () {
        return $('#size-empty-span').css('display') === 'block';
    };


    $scope.sortType = 'size';
    $scope.sortReverseName = false;
    $scope.sortReverseSize = false;
    $scope.sortReverseColor = false;
    $scope.sortReversePrice = false;
    $scope.sortReverse = false;

});



